Zadanie - aplikacja rekrutacyjna dla firmy ANSTA.

Aby uruchomić aplikację należy włączyć proces dla bazy danych mongo w terminalu za pomocą polecenia:

	mongod


następnie w innym okienku terminala wejść do folderu z projektem i wpisać komendę:

	npm install

a później:

	npm start

Aplikacja będzie dostępna pod adresem:

	http://localhost:3000

