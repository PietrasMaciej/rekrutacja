"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var router_1 = require('@angular/router');
var person_service_1 = require('./person.service');
var PersonsComponent = (function () {
    function PersonsComponent(router, personService) {
        this.router = router;
        this.personService = personService;
        this.persons = [];
        this.title = 'Zadanie rekrutacyjne';
    }
    PersonsComponent.prototype.onSelect = function (person) {
        this.selectedPerson = person;
    };
    PersonsComponent.prototype.getPersons = function () {
        var _this = this;
        this.personService.getPersons().subscribe(function (data) { return _this.persons = data; }, function (error) { return console.log(error); });
    };
    PersonsComponent.prototype.ngOnInit = function () {
        this.getPersons();
    };
    PersonsComponent.prototype.gotoDetail = function () {
        this.router.navigate(['/details', this.selectedPerson.surname]);
    };
    PersonsComponent = __decorate([
        core_1.Component({
            selector: 'persons',
            templateUrl: 'app/persons.component.html',
            styleUrls: ['app/persons.component.css']
        }), 
        __metadata('design:paramtypes', [router_1.Router, person_service_1.PersonService])
    ], PersonsComponent);
    return PersonsComponent;
}());
exports.PersonsComponent = PersonsComponent;
//# sourceMappingURL=persons.component.js.map