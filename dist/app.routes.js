"use strict";
var router_1 = require('@angular/router');
var persons_component_1 = require('./persons.component');
var person_detail_component_1 = require('./person-detail.component');
var routes = [
    {
        path: '',
        component: persons_component_1.PersonsComponent
    },
    {
        path: 'details/:surname',
        component: person_detail_component_1.PersonDetailComponent
    }
];
exports.appRouterProviders = [router_1.provideRouter(routes)];
//# sourceMappingURL=app.routes.js.map