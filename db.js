var mongoose = require("mongoose");

mongoose.connect('mongodb://localhost/data', function (error) {
  if (error) {
    console.log("No connection with database: " + error);
  } else {
    console.log("Connected to the database.");
  }
});

var person = mongoose.model('persons', new mongoose.Schema({
    _id: String,
    name: String,
    middleName: String,
    surname: String,
    PESEL: Number,
    NIP: Number,
    typeOfDocument: String,
    documentIssuedBy: String,
    documentNumber: String,
    dateOfBirth: String,
    birthplace: String,
    citizenship: String,
    nationality: String,
    nameOfFather: String,
    nameOfMother: String,
    maidenName: String,
    gender: String,
    education: String,
    phoneNumber: Number,
    email: String,
    postalCode: String,
    street: String,
    streetNumber: Number,
    houseNumber: Number,
    city: String
}));


var person1 = new person({
    _id: mongoose.Types.ObjectId(),
    name: 'Jan',
    middleName: 'Kazimierz',
    surname: 'Potocki',
    PESEL: 22120802098,
    NIP: 6617730660,
    typeOfDocument: 'Dowód osobisty',
    documentIssuedBy: 'Prezydenta miasta',
    documentNumber: 'AVD126115',
    dateOfBirth: '08-12-1922',
    birthplace: 'Sopot',
    citizenship: 'polskie',
    nationality: 'polska',
    nameOfFather: 'Zygmunt',
    nameOfMother: 'Ewa',
    maidenName: '',
    gender: 'Mężczyzna',
    education: 'Średnie',
    phoneNumber: 456364126,
    email: 'jankazik@test.com',
    postalCode: '56-167',
    street: 'Kamienna',
    streetNumber: 45,
    houseNumber: 3,
    city: 'Kraków'
  });

var person2 = new person({
    _id: mongoose.Types.ObjectId(),
    name: 'Marek',
    middleName: 'Wacław',
    surname: 'Kita',
    PESEL: 66101519531,
    NIP: 8932752435,
    typeOfDocument: 'Dowód osobisty',
    documentIssuedBy: 'Wójta',
    documentNumber: 'ASS548351',
    dateOfBirth: '15-10-1966',
    birthplace: 'Gdańsk',
    citizenship: 'polskie',
    nationality: 'polska',
    nameOfFather: 'Eustachy',
    nameOfMother: 'Weronika',
    maidenName: '',
    gender: 'Mężczyzna',
    education: 'Wyższe techniczne',
    phoneNumber: 653234873,
    email: 'marek12@test.pl',
    postalCode: '83-195',
    street: 'Śluza',
    streetNumber: 3,
    houseNumber: 25,
    city: 'Gdańsk'
  });

  var person3 = new person({
      _id: mongoose.Types.ObjectId(),
      name: 'Katarzyna',
      middleName: 'Magdalena',
      surname: 'Bartkowiak',
      PESEL: 92111307006,
      NIP: 6192017487,
      typeOfDocument: 'Dowód osobisty',
      documentIssuedBy: 'Burmistrza',
      documentNumber: 'AZC270024',
      dateOfBirth: '13-11-1992',
      birthplace: 'Kościerzyna',
      citizenship: 'polskie',
      nationality: 'polska',
      nameOfFather: 'Marek',
      nameOfMother: 'Karolina',
      maidenName: 'Bartkowiak',
      gender: 'Kobieta',
      education: 'Wyższe',
      phoneNumber: 755234754,
      email: 'katy@test.net',
      postalCode: '60-103',
      street: 'Głęboka',
      streetNumber: 78,
      houseNumber: 1,
      city: 'Kartuzy'
    });

    var person4 = new person({
        _id: mongoose.Types.ObjectId(),
        name: 'Tadeusz',
        middleName: 'Bogusław',
        surname: 'Paw',
        PESEL: 57060615451,
        NIP: 1868207249,
        typeOfDocument: 'Dowód osobisty',
        documentIssuedBy: 'Prezydenta miasta',
        documentNumber: 'AIH560336',
        dateOfBirth: '06-06-1957',
        birthplace: 'Szczecin',
        citizenship: 'polskie',
        nationality: 'polska',
        nameOfFather: 'Karol',
        nameOfMother: 'Justyna',
        maidenName: '',
        gender: 'Mężczyzna',
        education: 'Podstawowe',
        phoneNumber: 452889556,
        email: 'tadek@test.com',
        postalCode: '78-776',
        street: 'Wąska',
        streetNumber: 12,
        houseNumber: 12,
        city: 'Gdynia'
      });

      var person5 = new person({
          _id: mongoose.Types.ObjectId(),
          name: 'Zuzanna',
          middleName: 'Joanna',
          surname: 'Zynk',
          PESEL: 66082516204,
          NIP: 2128962601,
          typeOfDocument: 'Dowód osobisty',
          documentIssuedBy: 'Prezydenta miasta',
          documentNumber: 'AKW325749',
          dateOfBirth: '25-08-1966',
          birthplace: 'Zakopane',
          citizenship: 'polskie',
          nationality: 'polska',
          nameOfFather: 'Władysław',
          nameOfMother: 'Jadwiga',
          maidenName: 'Tart',
          gender: 'Kobieta',
          education: 'Wyższe',
          phoneNumber: 642766234,
          email: 'anna@test.com',
          postalCode: '34-334',
          street: 'Srebrna',
          streetNumber: 23,
          houseNumber: 5,
          city: 'Warlubie'
        });


var addPerson = function (personParam) {
    person.findOne ({
      surname: personParam.surname
    }, function(error, result) {
                 if(error) {
                   console.log(error);
                 } else {
                      if(result === null) {
                        personParam.save(function (err) {
                            if (err) {
                              console.error(err);
                            } else {
                              console.log("\nNew person\n" + personParam + "\nsaved in database.\n\n");
                            }
                        });
                      } else {
                          console.log("\nPerson\n" + personParam + "\nalready exists.\n\n");
                        }
                      }
      });
};

addPerson(person1);
addPerson(person2);
addPerson(person3);
addPerson(person4);
addPerson(person5);
