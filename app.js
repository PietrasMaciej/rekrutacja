require('./db.js');

var express = require('express');
var mongoose = require('mongoose');
var morgan = require('morgan');
var bodyParser = require('body-parser');

var app = express();

var port = process.env.PORT || 3000;

app.use(express.static(__dirname));

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));

app.use(morgan('dev'));

var persons = mongoose.model('persons');

app.get('/persons',function(req, res) {
  persons.find(function(error, result) {
    if(error) {
      console.error(error);
    } else {
      if(result === null) {
        console.log("No records in database.");
      } else {
        console.log(result);
        res.json(result);
        }
      }
  });
});

app.get('/*', function(req, res) {
        res.redirect('/');
});

app.listen(port, function () {
    console.log('Server works on port ' + port);
});
