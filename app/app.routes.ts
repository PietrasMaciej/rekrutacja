import { provideRouter, RouterConfig }  from '@angular/router';

import { PersonsComponent } from './persons.component';
import { PersonDetailComponent } from './person-detail.component';

const routes: RouterConfig = [
  {
    path: '',
    component: PersonsComponent
  },
  {
    path: 'details/:surname',
    component: PersonDetailComponent
  }
];

export const appRouterProviders = [ provideRouter(routes) ];
