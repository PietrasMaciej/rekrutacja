import { ActivatedRoute } from '@angular/router';

import { Component, OnInit, OnDestroy } from '@angular/core';
import { PersonService } from './person.service';


@Component({
  selector: 'my-person-detail',
  templateUrl: 'app/person-detail.component.html',
  styleUrls: ['app/person-detail.component.css']
})

export class PersonDetailComponent implements OnInit, OnDestroy {

person: Object;
sub: any;

constructor(private personService: PersonService, private route: ActivatedRoute) { }

ngOnInit() {
  this.sub = this.route.params.subscribe(params => {
  let id = params['surname'];
  this.personService.getPerson(id).subscribe(person => this.person = person);
  });
 }

 ngOnDestroy() {
  this.sub.unsubscribe();
}

goBack() {
  window.history.back();
}

}
