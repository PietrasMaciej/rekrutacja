import { Injectable } from '@angular/core';
import {Http} from '@angular/http';

@Injectable()
export class PersonService {

  constructor(private http: Http) { }

  getPerson(id: string) {
    return this.getPersons().map(persons => persons.find(person => person.surname === id));
  }

  getPersons() {
      return this.http.get("/persons").map(res => res.json());
  }

}
