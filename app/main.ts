import { bootstrap } from '@angular/platform-browser-dynamic';
import {HTTP_PROVIDERS} from '@angular/http';
import {enableProdMode} from '@angular/core';

import { AppComponent } from './app.component';
import { appRouterProviders } from './app.routes';

import 'rxjs/add/operator/map';

enableProdMode();

bootstrap(AppComponent, [ appRouterProviders, HTTP_PROVIDERS ]);
