import { Component }       from '@angular/core';
import { ROUTER_DIRECTIVES } from '@angular/router';

import { PersonService }     from './person.service';

@Component({
  selector: 'my-app',
  template: `
    <router-outlet></router-outlet>
  `,
  styleUrls: ['app/app.component.css'],
  directives: [ROUTER_DIRECTIVES],
  providers: [PersonService,]
})

export class AppComponent { }
