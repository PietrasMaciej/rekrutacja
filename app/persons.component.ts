import { Component } from '@angular/core';
import { Router } from '@angular/router';
import {Http} from '@angular/http';

import { PersonDetailComponent } from './person-detail.component';
import { PersonService } from './person.service';
import { OnInit } from '@angular/core';

@Component({
  selector: 'persons',
  templateUrl: 'app/persons.component.html',
  styleUrls: ['app/persons.component.css']
})
export class PersonsComponent implements OnInit {

  persons = [];

  title = 'Zadanie rekrutacyjne';
  selectedPerson: Object;

  constructor(private router: Router, private personService: PersonService) { }

  onSelect(person: Object) {
    this.selectedPerson = person;
  }

  getPersons() {
    this.personService.getPersons().subscribe(
    data => this.persons = data,
    error => console.log(error)
    );
  }

  ngOnInit() {
    this.getPersons();
  }

  gotoDetail() {
    this.router.navigate(['/details', this.selectedPerson.surname]);
  }

}
